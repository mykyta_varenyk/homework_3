package ua.dnipro.ntudp.varenyk_mykyta.library;

import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;

import java.time.LocalDateTime;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;

/**
 * Class for functions.tld(tag library)
 *
 * @author Mykyta Varenyk
 */
public final class Functions {
    private Functions() {}
    public static long getDelay(LocalDateTime approvedTime){
         return DAYS.between(approvedTime,LocalDateTime.now());
    }

    public static String concatFieldWithLanguage(String field, String language){
        return field + " " + language;
    }

    public static long getDelayHours(LocalDateTime approvedTime){
        return HOURS.between(approvedTime,LocalDateTime.now());
    }

    public static int countOrderedBooks(List<UserOrderBean> userOrderBeans,int id){
        int booksOrdered=0;
        for (UserOrderBean bean : userOrderBeans) {
            if (bean.getBookId() == id && !bean.isReturned()){
                booksOrdered++;
            }
        }
        return booksOrdered;
    }
}
