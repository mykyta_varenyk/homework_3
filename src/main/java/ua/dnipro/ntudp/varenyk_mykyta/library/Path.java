package ua.dnipro.ntudp.varenyk_mykyta.library;

/**
 * Path holder (jsp pages, controller commands).
 *
 * @author Mykyta Varenyk
 *
 */

public class Path {
    public static final String PAGE_LOGIN = "login";

    public static final String PAGE_ERROR_PAGE = "errors/error-page";

    public static final String PAGE_INDEX = "/index.jsp";

    public static final String PAGE_INDEX_REDIRECT = "redirect:/";

    public static final String PAGE_SEARCH = "search_results";

    public static final String PAGE_SEARCH_REDIRECT = "redirect:/FinalProject/controller/search";

    public static final String PAGE_ADMIN_HOME_PAGE = "admin/home_page";

    public static final String PAGE_ADMIN_HOME_PAGE_REDIRECT = "redirect:admin/home";

    public static final String PAGE_ADMIN_BOOKS_PAGE = "/admin/books";

    public static final String PAGE_ADMIN_BOOKS_PAGE_REDIRECT = "redirect:books";

    public static final String PAGE_ADMIN_BOOKS_PAGE_COMMAND = "/FinalProject/controller/admin/books";

    public static final String PAGE_ADMIN_BOOKS_PAGE_INCLUDE = "include:/FinalProject/controller/admin/books";

    public static final String PAGE_ADMIN_EDIT_A_BOOK_PAGE = "/jsp/admin/edit-book.jsp";

    public static final String PAGE_ADMIN_USERS_PAGE = "admin/users";

    public static final String PAGE_ADMIN_USERS_PAGE_REDIRECT = "redirect:/FinalProject/controller/admin/users";

    public static final String PAGE_USER_HOME_PAGE = "user/home_page";

    public static final String PAGE_USER_HOME_PAGE_REDIRECT = "redirect:user/home";

    public static final String PAGE_USER_ORDERS_PAGE = "user/orders";

    public static final String PAGE_USER_ORDERS_PAGE_REDIRECT = "redirect:/FinalProject/controller/user/orders";

    public static final String PAGE_USER_MAKE_ORDER_TAKE_HOME_PAGE = "/jsp/user/make_order_take_home.jsp";

    public static final String PAGE_USER_MAKE_ORDER_IN_READING_HALL_PAGE = "/jsp/user/make_order_in_reading_hall.jsp";

    public static final String PAGE_LIBRARIAN_HOME_PAGE = "librarian/home_page";

    public static final String PAGE_LIBRARIAN_HOME_PAGE_REDIRECT  = "redirect:librarian/home";

    public static final String PAGE_LIBRARIAN_LIST_USER_ORDERS = "/jsp/librarian/orders.jsp";

    public static final String PAGE_LIBRARIAN_LIST_USERS = "/jsp/librarian/users.jsp";

    public static final String PAGE_LIBRARIAN_LIST_ORDERS_FOR_USER = "/jsp/librarian/orders-for-user.jsp";

    public static final String PAGE_REGISTER = "/register.jsp";
}
