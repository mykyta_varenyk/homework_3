package ua.dnipro.ntudp.varenyk_mykyta.library.bpp;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Timed {
}
