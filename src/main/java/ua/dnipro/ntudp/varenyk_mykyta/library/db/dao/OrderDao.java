package ua.dnipro.ntudp.varenyk_mykyta.library.db.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.exceptions.RepositoryLayerException;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Order;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for Order entity and UserOrderBean bean.
 */
@Repository
public class OrderDao {
    private static Logger LOGGER = LogManager.getLogger(OrderDao.class);

    public static final String SQL_SELECT_ALL_ORDERS = "SELECT * FROM order";

    public static final String SQL_RETURN_ORDER= "UPDATE `order` SET is_returned=1 WHERE id=?;";

    public static final String SQL_GET_USER_ORDER_BEANS ="SELECT o.id, bt.publisher, bt.name, o.given_by_librarian, a.name as author, o.days_count, o.approved_time, o.hours_count, b.id as book_id, o.is_returned" +
            " FROM `order` o, order_has_book ohs, book b, book_has_author bha, author a, language l, book_translation bt" +
            " WHERE o.account_id=? AND l.languagecode=?  AND ohs.order_id=o.id AND bt.language_id=l.id AND b.id = bt.book AND b.id=ohs.book_id AND bha.book_Id=b.id AND a.id=bha.author_id AND a.lan_id=l.id LIMIT ?,?;";

    public static final String SQL_INSERT_ORDER_TAKE_HOME = "INSERT INTO `order` (account_id,days_count,in_reading_hall) VALUES (?,?,0)";

    public static final String SQL_CONNECT_ORDER_TO_BOOK = "INSERT INTO order_has_book VALUES (?,?)";

    public static final String SQL_SELECT_ALL_USER_ORDER_BEANS = "SELECT o.id, o.account_id, bt.publisher, bt.name,  a.name as author, o.days_count, o.hours_count, o.given_by_librarian, b.id as book_id, o.is_returned" +
            " FROM `order` o, order_has_book ohb, book b, book_has_author bha, author a, book_translation bt, language l" +
            " WHERE  ohb.order_id=o.id AND l.languagecode=? AND bt.language_id=l.id AND bt.book = b.id" +
            " AND b.id=ohb.book_id AND bha.book_Id=b.id AND a.id=bha.author_id AND a.lan_id=l.id ORDER BY o.id;";

    public static final String SQL_APPROVE_ORDER = "UPDATE `order` o, user u  SET o.given_by_librarian=?, o.is_approved=1, o.approved_time = NOW(), u.has_orders=1  WHERE o.id=? AND u.id = o.account_id;";

    public static final String SQL_DISAPPROVE_ORDER = "UPDATE `order` o SET given_by_librarian=?, is_approved = 0 WHERE o.id=?";

    private static final String SQL_INSERT_ORDER_IN_READING_HALL = "INSERT INTO `order` (account_id,hours_count,in_reading_hall) VALUES (?,?,1)";

    private static final String SQL_COUNT_ORDERS_FOR_USER = "SELECT COUNT(*) FROM `order` where account_id=?;";

    private DataSource dataSource;

    @Autowired
    public OrderDao(DataSource dataSource){
        this.dataSource = dataSource;
    }

    /**
     * Approve order.
     *
     * @param librarianId id of a librarian that approves an order
     *
     * @param orderId id of an order
     *
     * @return {@code true} if operation was successful,
     *{@code false} otherwise
     */
    public boolean approveOrder(int librarianId ,int orderId){
        boolean result = false;
        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_APPROVE_ORDER)){
            statement.setInt(1,librarianId);
            statement.setInt(2,orderId);
            result = statement.executeUpdate() == 2;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Disapprove order.
     *
     * @param librarianId id of a librarian that disapproves an order
     *
     * @param orderId id of an order
     *
     * @return {@code true} if operation was successful,
     * {@code false} otherwise
     */
    public boolean disapproveOrder(int librarianId, int orderId){
        boolean result = false;

        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_DISAPPROVE_ORDER)){
            statement.setInt(1,librarianId);
            statement.setInt(2,orderId);
            result = statement.executeUpdate() ==1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public boolean returnOrder(int orderId){
        boolean result = false;

        try(Connection con = dataSource.getConnection();
            PreparedStatement statement = con.prepareStatement(SQL_RETURN_ORDER)){
            statement.setInt(1,orderId);
            result = statement.executeUpdate() == 1;
        } catch (SQLException throwables) {
            LOGGER.debug("SQLException in OrderDao#returnOrder(int) -> {}",orderId);
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Returns all orders.
     *
     * @return List of all orders.
     */
    public List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<>();
        try(Connection con = dataSource.getConnection();
            Statement stmt = con.createStatement();
            ResultSet resSet = stmt.executeQuery(SQL_SELECT_ALL_ORDERS) ) {
            OrderDao.OrderMapper orderMapper = new OrderDao.OrderMapper();
            while (resSet.next()){
                System.out.println(orderMapper.mapRow(resSet));
                orders.add(orderMapper.mapRow(resSet));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return orders;
    }
    /**
     * Returns all order beans.
     *
     *
     * @param languageCode language code of internationalized fields
     *
     * @return List of all order beans.
     */
    public List<UserOrderBean> getAllUserOrderBeans(String languageCode){
        List<UserOrderBean> userOrderBeans= new ArrayList<>();

        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_SELECT_ALL_USER_ORDER_BEANS)){
        statement.setString(1,languageCode);
            try (ResultSet resultSet = statement.executeQuery()) {
                UserOrderBeanMapper mapper = new UserOrderBeanMapper();
                while (resultSet.next()) {
                    userOrderBeans.add(mapper.mapRow(resultSet));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return userOrderBeans;
    }

    /**
     * Insert "take home" order into DB.
     *
     * @param order order to be inserted.
     *
     * @return {@code true} if operation was successful,
     * {@code false} otherwise
     */
    public boolean createTakeHomeOrder(Order order){
        boolean result = false;

        try(Connection con = dataSource.getConnection();
            PreparedStatement statement = con.prepareStatement(SQL_INSERT_ORDER_TAKE_HOME,Statement.RETURN_GENERATED_KEYS)){
            statement.setInt(1,order.getAccountId());
            statement.setInt(2,order.getDaysCount());
            result = statement.executeUpdate() == 1;

            if (result){
                try(ResultSet resSet = statement.getGeneratedKeys()) {
                    int generatedId;
                    if (resSet.next()) {
                        generatedId = resSet.getInt(1);

                        try (PreparedStatement pStatement = con.prepareStatement(SQL_CONNECT_ORDER_TO_BOOK)) {
                            pStatement.setInt(1, generatedId);
                            pStatement.setInt(2, order.getBookId());
                            result = pStatement.executeUpdate() == 1;
                        }
                    }
                }
            }
        } catch (SQLException throwables) {
            throw new RepositoryLayerException(throwables.getMessage());
        }
        return result;
    }

    /**
     * Insert "in reading hall" order into DB.
     *
     * @param order order to be inserted.
     *
     * @return {@code true} if operation was successful,
     * {@code false} otherwise
     */
    public boolean createInReadingHallOrder(Order order){
        boolean result = false;

        try(Connection con = dataSource.getConnection();
            PreparedStatement statement = con.prepareStatement(SQL_INSERT_ORDER_IN_READING_HALL,Statement.RETURN_GENERATED_KEYS)){
            statement.setInt(1,order.getAccountId());
            statement.setInt(2,order.getHoursCount());
            result = statement.executeUpdate() == 1;

            System.out.println(result);
            if (result == true) {
                try (ResultSet resSet = statement.getGeneratedKeys()) {
                    int generatedId;
                    if (resSet.next()) {
                        generatedId = resSet.getInt(1);

                        try(PreparedStatement pStatement = con.prepareStatement(SQL_CONNECT_ORDER_TO_BOOK)){
                            pStatement.setInt(1,generatedId);
                            pStatement.setInt(2,order.getBookId());
                            result = pStatement.executeUpdate() == 1;
                        }
                    }
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Returns orders of a specified user with a limit and offset.
     *
     * Used in pair with OrderDao#getOrdersAmountForUser(int) to implement pagination.
     *
     * @param userId user id
     *
     * @param currentPage limit
     *
     * @param recordsPerPage offset
     *
     * @param languageCode a language code of internationalized fields
     *
     * @see OrderDao#getOrdersAmountForUser(int)
     *
     * @return List of orders.
     */
    public List<UserOrderBean> getUserOrderBeansByUserId(int userId,String languageCode,int currentPage,int recordsPerPage) {
        List<UserOrderBean> userOrderBeans = new ArrayList<>();

        int start = currentPage * recordsPerPage - recordsPerPage;

        try(Connection con = dataSource.getConnection();
            PreparedStatement statement = con.prepareStatement(SQL_GET_USER_ORDER_BEANS)) {
            statement.setInt(1,userId);
            statement.setString(2,languageCode);
            statement.setInt(3,start);
            statement.setInt(4,recordsPerPage);
            try(ResultSet resultSet = statement.executeQuery()){
              UserOrderBeanMapper mapper = new UserOrderBeanMapper();
                while (resultSet.next()){
                    UserOrderBean bean = mapper.mapRow(resultSet);
                    bean.setApprovedTime(resultSet.getObject(7, LocalDateTime.class));
                    userOrderBeans.add(bean);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return userOrderBeans;
    }

    /**
     * Returns the number of orders for a user.
     *
     * Used in pair with OrderDao#getUserOrderBeansByUserId(int, String, int, int)  to implement pagination.
     *
     * @param id user id
     *
     * @see OrderDao#getUserOrderBeansByUserId(int, String, int, int)
     *
     * @return Amount of orders of a user.
     */
    public int getOrdersAmountForUser(int id){
        int ordersAmount=0;
        try(Connection con = dataSource.getConnection();
        PreparedStatement statement = con.prepareStatement(SQL_COUNT_ORDERS_FOR_USER)){
            statement.setInt(1,id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    ordersAmount = resultSet.getInt(1);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ordersAmount;
    }

    /**
     * Extracts an  order entity from the result set row.
     */
    private static class OrderMapper implements EntityMapper<Order> {
        @Override
        public Order mapRow(ResultSet rs) {
            Order order = new Order();
            try {
                order.setId(rs.getInt(Fields.ENTITY_ID));
                order.setAccountID(rs.getInt(Fields.ORDER_USER_ID));
                order.setApproved(rs.getInt(Fields.ORDER_IS_APPROVED));
                order.setApprovedTime(rs.getDate(Fields.ORDER_APPROVED_TIME));
                order.setExpiredAt(rs.getDate(Fields.ORDER_EXPIRED_AT));
                order.setInReadingHall(rs.getInt(Fields.ORDER_IN_READING_HALL));
                order.setLibrarianId((long) rs.getInt(Fields.ORDER_GIVEN_BY_LIBRARIAN_ID));
                order.setReturned(rs.getInt(Fields.ORDER_IS_RETURNED));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return order;
        }
    }

    /**
     * Extracts an user order bean from the result set row.
     */
    private static class UserOrderBeanMapper implements EntityMapper<UserOrderBean> {
        @Override
        public UserOrderBean mapRow(ResultSet rs) {
            UserOrderBean bean = new UserOrderBean();
            try {
                bean.setOrderId(rs.getInt(Fields.ENTITY_ID));
                bean.setAuthor(rs.getString(Fields.BOOK_AUTHOR));
                bean.setHoursCount(rs.getInt(Fields.ORDER_HOURS_COUNT));
                bean.setLibrarianId(rs.getInt(Fields.ORDER_GIVEN_BY_LIBRARIAN_ID));
                bean.setName(rs.getString(Fields.ENTITY_NAME));
                bean.setPublisher(rs.getString(Fields.BOOK_PUBLISHER));
                bean.setDaysCount(rs.getInt(Fields.ORDER_DAYS_COUNT));
                bean.setBookId(rs.getInt(Fields.ORDER_HAS_BOOK_BOOK_ID));
                bean.setReturned(rs.getInt(Fields.ORDER_IS_RETURNED));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            return bean;
        }
    }

}
