package ua.dnipro.ntudp.varenyk_mykyta.library.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * Book entity.
 *
 * @author Mykyta Varenyk
 *
 */

public class Book extends Entity {

    private String name;
    private List<String> author;
    private LocalDate dateOfIssue;
    private String date;
    private String publisher;
    private String description;
    private int count;
    private String publisherUa;
    private String descriptionUa;
    private String nameUa;
    private boolean isReturned;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public String getPublisherUa() {
        return publisherUa;
    }

    public void setPublisherUa(String publisherUa) {
        this.publisherUa = publisherUa;
    }

    public String getDescriptionUa() {
        return descriptionUa;
    }

    public void setDescriptionUa(String descriptionUa) {
        this.descriptionUa = descriptionUa;
    }

    public String getNameUa() {
        return nameUa;
    }

    public void setNameUa(String nameUa) {
        this.nameUa = nameUa;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAuthor() {
        return author;
    }

    public void setAuthor(List<String> author) {
        this.author = author;
    }

    public LocalDate getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(LocalDate dateOfIssue) {

        this.dateOfIssue = dateOfIssue;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (this.getId() == book.getId()){
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        Object o = new Object();
        o.hashCode();
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author=" + author +
                ", dateOfIssue=" + dateOfIssue +
                ", date='" + date + '\'' +
                ", publisher='" + publisher + '\'' +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", publisherUa='" + publisherUa + '\'' +
                ", descriptionUa='" + descriptionUa + '\'' +
                ", nameUa='" + nameUa + '\'' +
                ", simpleDateFormat=" + simpleDateFormat +
                '}';
    }
}
