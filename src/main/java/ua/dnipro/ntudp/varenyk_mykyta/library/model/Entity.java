package ua.dnipro.ntudp.varenyk_mykyta.library.model;

import java.io.Serializable;

/**
 * Root of all entities which have identifier field.
 *
 * @author D.Kolesnikov
 *
 */
public class Entity implements Serializable {

    private static final long serialVersionUID = -6301425960673678047L;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
