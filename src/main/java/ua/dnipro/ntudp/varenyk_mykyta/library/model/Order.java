package ua.dnipro.ntudp.varenyk_mykyta.library.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

/**
 * Order entity.
 *
 * @author Mykyta Varenyk
 *
 */

public class Order extends Entity {
    private LocalDateTime approvedTime;
    private int accountId;
    private Long librarianId;
    private boolean isApproved;
    private boolean inReadingHall;
    private LocalDate expiredAt;
    private String expired;
    private int daysCount;
    private int hoursCount;
    private int bookId;
    private boolean isReturned;

    public boolean isReturned() {
        return isReturned;
    }

    public void setReturned(int isReturned) {
        if (isReturned == 1){
            this.isReturned = true;
        }else {
            this.isReturned = false;
        }
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getHoursCount() {
        return hoursCount;
    }

    public void setHoursCount(int hoursCount) {
        this.hoursCount = hoursCount;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public LocalDateTime getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(Date approved_time) {
        if (approved_time != null) {
            this.approvedTime = approved_time.
                    toInstant().
                    atZone(ZoneId.systemDefault()).
                    toLocalDateTime();
        }
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountID(int account_id) {
        this.accountId = account_id;
    }

    public Long getLibrarianId() {
        return librarianId;
    }

    public void setLibrarianId(Long librarianId) {
        this.librarianId = librarianId;
    }

    public boolean getIsApproved() {
        return isApproved;
    }

    public void setApproved(int approved) {
        if (approved == 1){
            isApproved = true;
        }
    }

    public boolean getIsInReadingHall() {
        return inReadingHall;
    }

    public void setInReadingHall(int inReadingHall) {
        if (inReadingHall == 1) {
            this.inReadingHall = true;
        }
    }

    public LocalDate getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Date expiredAt) {
        if (expiredAt != null) {
            this.expiredAt = expiredAt.toInstant().
                    atZone(ZoneId.systemDefault()).
                    toLocalDate();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return accountId == order.accountId &&
                isApproved == order.isApproved &&
                inReadingHall == order.inReadingHall &&
                daysCount == order.daysCount &&
                hoursCount == order.hoursCount &&
                bookId == order.bookId &&
                Objects.equals(approvedTime, order.approvedTime) &&
                Objects.equals(librarianId, order.librarianId) &&
                Objects.equals(expiredAt, order.expiredAt) &&
                Objects.equals(expired, order.expired);
    }

    @Override
    public int hashCode() {
        return Objects.hash(approvedTime, accountId, librarianId, isApproved, inReadingHall, expiredAt, expired, daysCount, hoursCount, bookId);
    }

    @Override
    public String toString() {
        return "Order{" +
                "approved_time=" + approvedTime +
                ", account_id=" + accountId +
                ", librarianId=" + librarianId +
                ", isApproved=" + isApproved +
                ", inReadingHall=" + inReadingHall +
                ", expiredAt=" + expiredAt +
                '}';
    }
}
