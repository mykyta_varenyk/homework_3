package ua.dnipro.ntudp.varenyk_mykyta.library.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.command.Command;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.command.CommandContainer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main servlet controller.
 *
 * @author Mykyta Varenyk
 */
public class Controller extends HttpServlet {

    private static final long serialVersionUID = 1859530046914824068L;

    private static Logger LOG = LogManager.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("do get method of Controller invoked");
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("do post method of Controller invoked");
        process(req, resp);
    }

    private void process(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String commandName = req.getParameter("command");

        if (commandName == null){
            commandName = cleanPath(req.getRequestURI());
            LOG.debug("command -> {}",commandName);
        }
        Command command = CommandContainer.getCommand(commandName);
        String forward = command.execute(req,resp);

        if (forward.contains("redirect:")){
            LOG.debug("page redirected");
            LOG.debug("Controller finished");
            resp.sendRedirect(forward.replace("redirect:",""));
        } else {
            LOG.debug("page forwarded");
            LOG.debug("Controller finished");
            req.getRequestDispatcher(forward).forward(req, resp);
        }

    }

    private String cleanPath(String path){
        return path.replaceAll(".*/controller/","");
    }
}

