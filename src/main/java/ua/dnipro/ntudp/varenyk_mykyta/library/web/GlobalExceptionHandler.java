package ua.dnipro.ntudp.varenyk_mykyta.library.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;

import java.io.IOException;
import java.sql.SQLException;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LogManager.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleError(NullPointerException exception){
        ModelAndView modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

        modelAndView.addObject("code",HttpStatus.INTERNAL_SERVER_ERROR.value());

        modelAndView.addObject("errorMessage","unexpected internal server error");

        LOGGER.debug("exception of type",exception);

        return modelAndView;
    }

    @ExceptionHandler(value =  IOException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleError(IOException exception){
        ModelAndView modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

        modelAndView.addObject("code",HttpStatus.INTERNAL_SERVER_ERROR.value());

        modelAndView.addObject("errorMessage","unexpected internal server error");

        LOGGER.debug("exception of type",exception);

        return modelAndView;
    }

    @ExceptionHandler(value =  SQLException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleError(SQLException exception){
        ModelAndView modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

        modelAndView.addObject("code",HttpStatus.INTERNAL_SERVER_ERROR.value());

        modelAndView.addObject("errorMessage","unexpected internal server error");

        LOGGER.debug("exception of type",exception);

        return modelAndView;
    }
}
