package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.util.List;

/**
 * Change language.
 *
 * @author Mykyta Varenyk
 *
 */

@Service
public class ChangeLanguageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(ChangeLanguageCommand.class);

    private BookDao bookDao;

    @Autowired
    public ChangeLanguageCommand(BookDao bookDao){
        this.bookDao = bookDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("ChangeLanguageCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = req.getParameter("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        List<Author> authors = bookDao.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}",authors);

        List<Book> booksList = bookDao.getAllBooks(languageCode);

        LOGGER.debug("books -> {}",booksList);

        List<String> publishers = bookDao.getAllPublishers(languageCode);

        LOGGER.debug("publisher names -> {}",publishers);

        Config.set(session,Config.FMT_LOCALE,languageCode);

        session.setAttribute("authors",authors);
        session.setAttribute("language",languageCode);
        session.setAttribute("booksList",booksList);
        session.setAttribute("publishers",publishers);
        session.setAttribute("bookId",req.getParameter("id"));

        LOGGER.debug("ChangeLanguageCommand finished");

        return req.getParameter("pageToReturn");
    }
}
