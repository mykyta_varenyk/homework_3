package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main interface for the Command pattern implementation.
 *
 * @author D.Kolesnikov
 *
 */
public abstract class Command {

    /**
     * Execution method for command.
     * @return Address to go once the command is executed.
     */
    public abstract String execute(HttpServletRequest req, HttpServletResponse resp);

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
