package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Delete a book.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class DeleteBookCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(DeleteBookCommand.class);

    private BookDao bookDao;

    @Autowired
    public DeleteBookCommand(BookDao bookDao){
        this.bookDao = bookDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("DeleteBookCommand started");

        int id = Integer.parseInt(req.getParameter("id"));

        LOGGER.debug("book id -> {}",id);

        try {
            bookDao = new BookDao(DBManager.getDataSource());
        } catch (NamingException e){
            LOGGER.error("NamingException in DeleteBookCommand -> {}",e);
        }

        boolean result = bookDao.deleteBookById(id);

        LOGGER.debug("book deleted -> {}",result);

        LOGGER.debug("DeleteBookCommand finished");
        return Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT;
    }
}
