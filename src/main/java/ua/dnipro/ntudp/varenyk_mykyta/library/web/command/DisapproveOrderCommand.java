package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Disapprove an order.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class DisapproveOrderCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(DisapproveOrderCommand.class);

    private OrderDao orderDao;

    @Autowired
    public DisapproveOrderCommand(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("DisapproveOrderCommand started");

        HttpSession session = req.getSession();

        User librarian = (User) session.getAttribute("user");

        int librarianId = librarian.getId();

        LOGGER.debug("id of a librarian, that disapproves order -> {}",librarianId);

        int id = Integer.parseInt(req.getParameter(Fields.ENTITY_ID));

        LOGGER.debug("order id -> {}",id);

        try {
            orderDao = new OrderDao(DBManager.getDataSource());
        } catch (NamingException e) {
            LOGGER.error("NamingException in DisapproveOrderCommand -> {}",e);
        }

        boolean result = orderDao.disapproveOrder(librarianId,id);

        LOGGER.debug("order disapproved -> {}",result);

        return Path.PAGE_LIBRARIAN_LIST_USER_ORDERS;
    }
}
