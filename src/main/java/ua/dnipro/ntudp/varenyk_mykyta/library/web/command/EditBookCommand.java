package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Edit a book.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class EditBookCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(EditBookCommand.class);

    private BookDao bookDao;

    @Autowired
    public EditBookCommand(BookDao bookDao){
        this.bookDao = bookDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("EditBookCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        try {
            bookDao = new BookDao(DBManager.getDataSource());
        } catch (NamingException e) {
            LOGGER.error("NamingException in EditBookCommand -> {}",e);
        }

        int bookId = Integer.parseInt(req.getParameter("bookId"));

        LOGGER.debug("bookId -> {}", bookId);

        Book book = getBook(req);

        book.setId(bookId);

        boolean result = bookDao.updateBook(book,Integer.parseInt(req.getParameter("authorId")));

        LOGGER.debug("book updated -> {}",result);

        return Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT;
    }

    private Book getBook(HttpServletRequest request){
        Book book = new Book();
        book.setDate(request.getParameter(Fields.BOOK_DATE_OF_ISSUE));
        book.setCount(Integer.parseInt(request.getParameter(Fields.BOOK_COUNT)));
        book.setName(request.getParameter(Fields.ENTITY_NAME));
        book.setDescription(request.getParameter(Fields.BOOK_DESCRIPTION));
        book.setPublisher(request.getParameter(Fields.BOOK_PUBLISHER));
        book.setNameUa(request.getParameter(Fields.BOOK_NAME_UA));
        book.setDescriptionUa(request.getParameter(Fields.BOOK_DESCRIPTION_UA));
        book.setPublisherUa(request.getParameter(Fields.BOOK_PUBLISHER_UA));
        return book;
    }

}
