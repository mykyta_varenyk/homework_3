package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Return a books page of admin user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetBooksPageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetBooksPageCommand.class);

    private BookDao bookDao;

    @Autowired
    public GetBooksPageCommand(BookDao bookDao){
        this.bookDao = bookDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetBooksPageCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        } else {
            languageCode = "en";
        }

        if (req.getParameter("dateOfIssueInTheFuture") != null){
            LOGGER.debug(req.getParameter("dateOfIssueInTheFuture"));
            req.setAttribute("dateOfIssueInTheFuture",req.getParameter("dateOfIssueInTheFuture"));
        }

        LOGGER.debug("language code -> {}",languageCode);

        int currentPage = 1;

        int recordsPerPage = 2;

        if (req.getParameter("page") != null){
            currentPage = Integer.parseInt(req.getParameter("page"));
        }

        LOGGER.debug("currentPage -> {}",currentPage);

        List<Book> books = bookDao.getBooks(currentPage,recordsPerPage,languageCode);

        LOGGER.debug("books with specified limit:{} and offset:{} -> {}",currentPage,recordsPerPage,books);

        int rows = bookDao.getNumberOfBooks();

        int numOfPages = (int) Math.ceil(rows * 1.0/recordsPerPage);

        LOGGER.debug("numOfPages -> {}",numOfPages);

        req.setAttribute("books",books);

        List<Author> authors = bookDao.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}",authors);

        List<String> languages = DBManager.getLanguages();

        LOGGER.debug("languages -> {}",languages);

        req.setAttribute("currentPage",currentPage);

        req.setAttribute("recordsPerPage",recordsPerPage);

        req.setAttribute("numOfPages",numOfPages);

        req.setAttribute("languages", languages);

        req.setAttribute("authors",authors);

        LOGGER.debug("GetBooksPageCommand finished");

        return Path.PAGE_ADMIN_BOOKS_PAGE;
    }
}
