package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Return a home page of librarian user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetLibrarianHomePageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetLibrarianHomePageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetLibrarianHomePageCommand executed");
        return Path.PAGE_LIBRARIAN_HOME_PAGE;
    }
}
