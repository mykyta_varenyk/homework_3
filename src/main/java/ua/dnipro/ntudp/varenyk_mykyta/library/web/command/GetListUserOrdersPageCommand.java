package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Return a books page of admin user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetListUserOrdersPageCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(GetListUserOrdersPageCommand.class);

    private OrderDao orderDao;

    @Autowired
    public GetListUserOrdersPageCommand(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetListUserOrdersPageCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans(languageCode);

        LOGGER.debug("userOrderBeans -> {}",userOrderBeans);

        req.setAttribute("userOrderBeans",userOrderBeans);

        LOGGER.debug("GetListUserOrdersPageCommand finished");
        return Path.PAGE_LIBRARIAN_LIST_USER_ORDERS;
    }
}
