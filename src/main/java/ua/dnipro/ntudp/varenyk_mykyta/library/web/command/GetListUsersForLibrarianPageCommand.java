package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Returns a page with users(except admin) for librarian user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetListUsersForLibrarianPageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetListUsersForLibrarianPageCommand.class);

    private UserDao userDao;

    @Autowired
    public GetListUsersForLibrarianPageCommand(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetListUsersForLibrarianPageCommand started");

        List<User> users = userDao.getAllUsers();

        LOGGER.debug("users -> {}",users);

        req.setAttribute("users",users);

        LOGGER.debug("GetListUsersForLibrarianPageCommand finished");

        return Path.PAGE_LIBRARIAN_LIST_USERS;
    }
}
