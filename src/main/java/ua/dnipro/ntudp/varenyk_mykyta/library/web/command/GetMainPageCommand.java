package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
@Service
public class GetMainPageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetMainPageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetMainPageCommand started");

        String languageCode;

        HttpSession session = req.getSession();

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        BookDao bookDao = null;
        try {
            bookDao = new BookDao(DBManager.getDataSource());
        } catch (NamingException e) {
            LOGGER.error("NamingException in GetMainPageCommand -> {}",e);
            e.printStackTrace();
        }

        List<Author> authors = bookDao.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}",authors);

        req.setAttribute("authors",authors);

        List<Book> books = bookDao.getAllBookNames(languageCode);

        LOGGER.debug("book names -> {}",books);

        req.setAttribute("books",books);

        LOGGER.debug("GetMainPageCommand finished");
        return Path.PAGE_INDEX_REDIRECT;
    }
}
