package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Returns a page with ordered books  for a user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetOrderedBooksPageCommand extends Command{

    private static Logger LOGGER = LogManager.getLogger(GetOrderedBooksPageCommand.class);

    private OrderDao orderDao;

    @Autowired
    public GetOrderedBooksPageCommand(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetOrderedBooksPageCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        int currentPage = 1;

        int recordsPerPage = 2;

        if (req.getParameter("page") != null){
            currentPage = Integer.parseInt(req.getParameter("page"));
        }

        LOGGER.debug("currentPage -> {}",currentPage);

        User user = (User) session.getAttribute("user");

        int id = user.getId();

        LOGGER.debug("user id -> {}",id);

        int rows = orderDao.getOrdersAmountForUser(id);

        LOGGER.debug("rows -> {}",rows);

        int numOfPages = (int) Math.ceil(rows * 1.0/recordsPerPage);

        LOGGER.debug("numOfPages -> {}",numOfPages);

        List<UserOrderBean> userOrderBeans = orderDao.getUserOrderBeansByUserId(id,languageCode,currentPage,recordsPerPage);

        LOGGER.debug("userOrderBeans with limit:{} and offset:{} -> {}",currentPage,recordsPerPage,userOrderBeans);

        req.setAttribute("userOrderBeans",userOrderBeans);

        req.setAttribute("currentPage",currentPage);

        req.setAttribute("recordsPerPage",recordsPerPage);

        req.setAttribute("numOfPages",numOfPages);

        LOGGER.debug("GetOrderedBooksPageCommand finished");

        return Path.PAGE_USER_ORDERS_PAGE;
    }
}
