package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Returns a home page for a user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetUserHomePageCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(GetUserHomePageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetUserHomePageCommand executed");

        return Path.PAGE_USER_HOME_PAGE;
    }
}
