package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Logout command.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class LogoutCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("LogoutCommand started");

        HttpSession session = req.getSession(false);

        if (session != null) {
            session.invalidate();
            LOGGER.debug("Session invalidated");
        }else{
            LOGGER.debug("there is no active session");
        }

        LOGGER.debug("LogoutCommand finished");

        return Path.PAGE_INDEX_REDIRECT;
    }
}
