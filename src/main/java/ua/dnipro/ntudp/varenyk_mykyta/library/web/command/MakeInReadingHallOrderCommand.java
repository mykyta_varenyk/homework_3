package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Order;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Confirm a reading hall order.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class MakeInReadingHallOrderCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(MakeInReadingHallOrderCommand.class);

    private OrderDao orderDao;

    @Autowired
    public MakeInReadingHallOrderCommand(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("MakeOrderInReadingHallCommand started");

        HttpSession session = req.getSession();

        User user = (User) session.getAttribute("user");

        int userId = user.getId();

        LOGGER.debug("user id -> {}",userId);

        Order order = getOrder(req);

        order.setAccountID(userId);

        boolean result = orderDao.createInReadingHallOrder(order);

        LOGGER.debug("order in reading hall created -> {}",result);

        LOGGER.debug("MakeOrderInReadingHallCommand finished");

        return Path.PAGE_USER_HOME_PAGE_REDIRECT;
    }

    private Order getOrder(HttpServletRequest request){
        Order order = new Order();

        order.setBookId(Integer.parseInt(request.getParameter(Fields.ENTITY_ID)));

        order.setHoursCount(Integer.parseInt(request.getParameter(Fields.ORDER_HOURS_COUNT)));

        return order;
    }
}
