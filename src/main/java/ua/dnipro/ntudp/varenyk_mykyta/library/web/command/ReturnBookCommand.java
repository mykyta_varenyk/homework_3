package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Return book.
 *
 * @author Mykyta Varenyk
 */
@Service
public class ReturnBookCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(ReturnBookCommand.class);

    private OrderDao orderDao;

    @Autowired
    public ReturnBookCommand(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("ReturnBookCommand started");

        int orderId = Integer.parseInt(req.getParameter(Fields.ENTITY_ID));

        LOGGER.debug("id of an order to be returned -> {}",orderId);

        boolean result = orderDao.returnOrder(orderId);

        LOGGER.debug("book returned -> {}",result);

        LOGGER.debug("ReturnBookCommand finished");
        return Path.PAGE_USER_ORDERS_PAGE_REDIRECT;
    }
}
