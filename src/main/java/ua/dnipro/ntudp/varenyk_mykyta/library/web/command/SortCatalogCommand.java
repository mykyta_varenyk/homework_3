package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

/**
 * Sort catalog.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class SortCatalogCommand extends Command {
    private static Logger LOG = LogManager.getLogger(SortCatalogCommand.class);

    public void sortByName(List<Book> books){
        Collections.sort(books,(b1, b2) -> b1.getName().compareTo(b2.getName()));
    }

    public void sortByAuthor(List<Book> books){
        Collections.sort(books,(b1,b2) -> {
            int result = b1.getAuthor().get(0).
                    compareTo(b2.getAuthor().get(0));
            if (result == 0){
                return b1.getName().compareTo(b2.getName());
            }
            return result;
        });
    }

    public void sortByPublisher(List<Book> books){
        Collections.sort(books,(b1,b2) -> {
            int result = b1.getPublisher().compareTo(b2.getPublisher());
            if (result == 0){
                return b1.getName().compareTo(b2.getName());
            }
            return result;
        });
    }

    public void sortByDateOfIssue(List<Book> books){
        Collections.sort(books,(b1,b2) -> {
            int result = b1.getDateOfIssue().compareTo(b2.getDateOfIssue());

            if (result == 0){
                return b1.getName().compareTo(b2.getName());
            }

            return result;
        });
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOG.debug("SortCatalogCommand started");

        HttpSession session = req.getSession();

        String sortBy = req.getParameter("sort_by");

        LOG.debug("selected sorting parameter -> {}",sortBy);

        List<Book> booksList = (List<Book>) session.getAttribute("booksList");

        LOG.debug("presorted books -> {}",booksList);

        if ("name".equals(sortBy)){
            sortByName(booksList);
        }else if ("author".equals(sortBy)){
           sortByAuthor(booksList);
        }else if ("publisher".equals(sortBy)){
            sortByPublisher(booksList);
        }else if("date-of-issue".equals(sortBy)){
            sortByDateOfIssue(booksList);
        }

        LOG.debug("sorted catalog -> {}",booksList);

        req.getSession().setAttribute("booksList",booksList);

        LOG.debug("SortCatalogCommand finished");

        return Path.PAGE_INDEX_REDIRECT;
    }
}
