package ua.dnipro.ntudp.varenyk_mykyta.library.web.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Role;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Security filter.
 *
 * @author Mykyta Varenyk
 */

public class AccessFilter implements Filter {
    private static Map<Role,List<String>> accessMap =new HashMap<>();
    private static List<String> commons = new ArrayList<>();
    private static List<String> outOfControl = new ArrayList<>();

    private static Logger LOGGER = LogManager.getLogger(AccessFilter.class);


    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        LOGGER.debug("Filter starts");
        if (accessAllowed(req)) {
            LOGGER.debug("Filter finished");
            chain.doFilter(req, resp);
        }else {
            String errorMessage = "You have no permission to access this resource";
            req.setAttribute("errorMessage",errorMessage);
            LOGGER.debug("errorMessage -> {}",errorMessage);
            req.getRequestDispatcher(Path.PAGE_ERROR_PAGE)
                    .forward(req,resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {
        LOGGER.debug("Filter initialization starts");

        accessMap.put(Role.ADMIN,asList(config.getInitParameter("admin")));

        accessMap.put(Role.USER,asList(config.getInitParameter("user")));

        accessMap.put(Role.LIBRARIAN,asList(config.getInitParameter("librarian")));

        LOGGER.debug("Access map -> {}",accessMap);

        commons = asList(config.getInitParameter("common"));

        LOGGER.debug("Command commons -> {}",commons);

        outOfControl = asList(config.getInitParameter("out-of-control"));

        LOGGER.debug("Out of control commands -> {}",outOfControl);

        LOGGER.debug("Filter initialization finished");
    }

    public boolean accessAllowed(ServletRequest request){
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        String commandName = cleanPath(((HttpServletRequest) request).getRequestURI());

        LOGGER.debug("requested command -> {}",commandName);

        if (commandName == null || commandName.isEmpty())
            return false;

        if (outOfControl.contains(commandName)) {
            return true;
        }

        HttpSession session = httpServletRequest.getSession(false);

        if (session == null)
            return false;

        Role userRole = (Role) session.getAttribute("userRole");

        LOGGER.debug("userRole -> {}",userRole);

        if (userRole == null)
            return false;

        return accessMap.get(userRole).contains(commandName)
                || commons.contains(commandName);
    }

    private List<String> asList(String str){
        List<String> list = new LinkedList<>();
        Pattern pattern = Pattern.compile("\\w+([\\/]\\w+([\\-\\w+]\\w+([\\-\\w+]\\w+([\\-\\w+]\\w+([\\-\\w+]\\w+[\\-\\w+]\\w+|\\w+))|\\w+)|\\w+)|\\-\\w+|\\w+)");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()){
            list.add(matcher.group());
        }
        return list;
    }

    private String cleanPath(String path){
        return path.replaceAll(".*/controller/","");
    }
}
