package ua.dnipro.ntudp.varenyk_mykyta.library.web.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Context listener.
 *
 * @author Mykyta Varenyk
 *
 */

public class ContextListener implements ServletContextListener {
    private static Logger LOGGER = LogManager.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log("Servlet context initialization starts");

        ServletContext servletContext = sce.getServletContext();

        initI18N(servletContext);

        log("Servlet context initialization finished");
    }

    private void initI18N(ServletContext servletContext){
        LOGGER.debug("I18N initialization started");

        String localesValues = servletContext.getInitParameter("locales");

        if (localesValues == null || localesValues.isEmpty()){
            LOGGER.warn("'locales' init parameter is empty, the default encoding will be used");
        }else {
            List<String> locales = new ArrayList<>();

            Pattern pattern = Pattern.compile("\\w{2,}");

            Matcher matcher = pattern.matcher(localesValues);

            while (matcher.find()){
                locales.add(matcher.group());
            }

            LOGGER.debug("Application attribute set -> {}",locales);
            servletContext.setAttribute("locales",locales);
            LOGGER.debug("Default locale -> {}",servletContext.getInitParameter("locale"));
        }
        LOGGER.debug("I18N initialization finished");
    }

    private void log(String msg) {
        System.out.println("[ContextListener] " + msg);
    }
}
