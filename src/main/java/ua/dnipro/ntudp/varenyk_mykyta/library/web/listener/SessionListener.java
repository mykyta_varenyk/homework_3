package ua.dnipro.ntudp.varenyk_mykyta.library.web.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.naming.NamingException;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Session listener.
 *
 * @author Mykyta Varenyk
 */

public class SessionListener implements HttpSessionListener {

    private static final Logger LOGGER = LogManager.getLogger(SessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        LOGGER.debug("Session started");

        HttpSession session = se.getSession();

        String languageCode = session.getServletContext().getInitParameter("locale");

        LOGGER.debug("languageCode from ServletContext -> {}",languageCode);

        BookDao bookDao = null;
        OrderDao orderDao = null;
        try {
            bookDao = new BookDao(DBManager.getDataSource());
            orderDao = new OrderDao(DBManager.getDataSource());
        } catch (NamingException e) {
            LOGGER.error("NamingException in SessionListener -> {}",e);
            e.printStackTrace();
        }
        List<Author> authors = bookDao.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {} ",authors);

        List<Book> books = bookDao.getAllBookNames(languageCode);

        LOGGER.debug("book names -> {}",books);

        List<Book> booksList = bookDao.getAllBooks(languageCode);

        LOGGER.debug("books -> {}",booksList);

        List<String> publishers = bookDao.getAllPublishers(languageCode);

        LOGGER.debug("publishers -> {}",publishers);

        List<LocalDateTime> datesOfIssue = bookDao.getAllDatesOfIssue();

        LOGGER.debug("datesOfIssue -> {}", datesOfIssue);

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans(languageCode);

        session.setAttribute("authors",authors);

        session.setAttribute("books",books);

        session.setAttribute("booksList",booksList);

        session.setAttribute("publishers",publishers);

        session.setAttribute("datesOfIssue",datesOfIssue);

        session.setAttribute("language",languageCode);

        session.setAttribute("allOrders",userOrderBeans);

        LOGGER.debug("Session initialized");
    }
}
