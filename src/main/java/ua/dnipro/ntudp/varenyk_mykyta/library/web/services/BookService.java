package ua.dnipro.ntudp.varenyk_mykyta.library.web.services;

import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BookService {
    void sortByName(List<Book> books);

    void sortByAuthor(List<Book> books);

    void sortByPublisher(List<Book> books);

    void sortByDateOfIssue(List<Book> books);

    List<Book> findBooks(String languageCode, String searchByAuthor, String searchByName, String authorId, String nameId);

    List<Book> sortBooks(List<Book> booksList, String sortBy);

    void changeLanguage(HttpServletRequest request);

    List<Book> getBooksWithPagination(int currentPage, int recordsPerPage, String languageCode);

    List<Author> getAllAuthors(String languageCode);

    int getAmountOfBooks();

    void createBook(HttpServletRequest req, int id, String languageCode);
}
