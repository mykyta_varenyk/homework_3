package ua.dnipro.ntudp.varenyk_mykyta.library.web.services;

import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;

import java.util.List;

public interface OrderService {
    int countOrdersByUserId(int id, int recordsPerPage);

    List<UserOrderBean> getUserOrderBeansWithPagination(int currentPage, int recordsPerPage, String languageCode, int id);
}
