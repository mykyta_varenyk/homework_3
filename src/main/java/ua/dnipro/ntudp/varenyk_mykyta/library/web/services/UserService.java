package ua.dnipro.ntudp.varenyk_mykyta.library.web.services;

import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface UserService {
    ModelAndView validateLoginInformation(String login, String password, String languageCode, HttpSession session);

    List<User> getUsersWithPagination(int currentPage, int recordsPerPage);

    int countUsers();
}
