package ua.dnipro.ntudp.varenyk_mykyta.library.web.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.bpp.Timed;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.exceptions.ServiceLayerException;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.util.Collections;
import java.util.List;

@Service
@Timed
public class BookServiceImpl implements BookService {
    private static Logger LOGGER = LogManager.getLogger(BookServiceImpl.class);
    private BookDao bookDao;

    @Override
    public void sortByName(List<Book> books){
        Collections.sort(books,(b1, b2) -> b1.getName().compareTo(b2.getName()));
    }

    @Override
    public void sortByAuthor(List<Book> books){
        Collections.sort(books,(b1,b2) -> {
            int result = b1.getAuthor().get(0).
                    compareTo(b2.getAuthor().get(0));
            if (result == 0){
                return b1.getName().compareTo(b2.getName());
            }
            return result;
        });
    }

    @Override
    public void sortByPublisher(List<Book> books){
        Collections.sort(books,(b1,b2) -> {
            int result = b1.getPublisher().compareTo(b2.getPublisher());
            if (result == 0){
                return b1.getName().compareTo(b2.getName());
            }
            return result;
        });
    }

    @Override
    public void sortByDateOfIssue(List<Book> books){
        Collections.sort(books,(b1,b2) -> {
            int result = b1.getDateOfIssue().compareTo(b2.getDateOfIssue());

            if (result == 0){
                return b1.getName().compareTo(b2.getName());
            }

            return result;
        });
    }

    @Autowired
    public BookServiceImpl(BookDao bookDao){
        this.bookDao = bookDao;
    }

    @Override
    public List<Book> findBooks(String languageCode, String searchByAuthor, String searchByName, String authorId, String nameId){
        List<Book> books;

        LOGGER.debug("searchByAuthor -> {} and searchByName -> {}",searchByAuthor,searchByName);

        if (searchByAuthor != null && searchByAuthor.equals("yes") && searchByName != null && searchByName.equals("yes")) {
            books = bookDao.
                    findBookBeansByNameAndAuthor(languageCode,
                            String.valueOf(authorId),
                            String.valueOf(nameId));
        } else if (searchByAuthor != null && searchByName == null) {
            books = bookDao.
                    findBookBeansByAuthors(languageCode, authorId);
        } else if (searchByAuthor == null && searchByName != null) {
            books = bookDao.
                    findBookBeansByName(languageCode, nameId);
        }else {
            books = null;
        }
        LOGGER.debug("books found -> {}", books);

        return books;
    }

    @Override
    public List<Book> sortBooks(List<Book> booksList, String sortBy){
        if ("name".equals(sortBy)){
            sortByName(booksList);
        }else if ("author".equals(sortBy)){
            sortByAuthor(booksList);
        }else if ("publisher".equals(sortBy)){
            sortByPublisher(booksList);
        }else if("date-of-issue".equals(sortBy)) {
            sortByDateOfIssue(booksList);
        }
        return booksList;
    }

    @Override
    public void changeLanguage(HttpServletRequest request){
        HttpSession session = request.getSession();

        String languageCode = Util.getLanguageCodeFromSession(session);

        List<Author> authors = bookDao.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}",authors);

        List<Book> booksList = bookDao.getAllBooks(languageCode);

        LOGGER.debug("books -> {}",booksList);

        List<String> publishers = bookDao.getAllPublishers(languageCode);

        LOGGER.debug("publisher names -> {}",publishers);

        Config.set(session,Config.FMT_LOCALE,languageCode);

        session.setAttribute("authors",authors);

        session.setAttribute("language",languageCode);

        session.setAttribute("booksList",booksList);

        session.setAttribute("publishers",publishers);

        session.setAttribute("bookId",request.getParameter("id"));
    }

    @Override
    public List<Book> getBooksWithPagination(int currentPage, int recordsPerPage, String languageCode){
        List<Book> books = bookDao.getBooks(currentPage,recordsPerPage,languageCode);

        LOGGER.debug("books with specified limit:{} and offset:{} -> {}",currentPage,recordsPerPage,books);

        int rows = bookDao.getNumberOfBooks();

        int numOfPages = (int) Math.ceil(rows * 1.0/recordsPerPage);

        LOGGER.debug("numOfPages -> {}",numOfPages);

        return books;
    }

    @Override
    public List<Author> getAllAuthors(String languageCode){
        return bookDao.getAllAuthors(languageCode);
    }

    @Override
    public int getAmountOfBooks(){
        return bookDao.getNumberOfBooks();
    }

    @Override
    public void createBook(HttpServletRequest req, int id, String languageCode){
        Book book = getBook(req,id,languageCode);

        LOGGER.debug("book to insert -> {}", book);

        boolean result = bookDao.createBook(book, id);

        LOGGER.debug("book inserted -> {}", result);
    }

    private Book getBook(HttpServletRequest request, int id, String languageCode){
        Book book = new Book();

        book.setAuthor(Collections.singletonList(bookDao.getAuthorById(id,languageCode)));
        book.setDescription(request.getParameter(Fields.BOOK_DESCRIPTION + " English"));
        book.setDescriptionUa(request.getParameter(Fields.BOOK_DESCRIPTION + " Ukrainian"));
        book.setDate(request.getParameter(Fields.BOOK_DATE_OF_ISSUE));
        book.setCount(Integer.parseInt(request.getParameter(Fields.BOOK_COUNT)));
        book.setPublisher(request.getParameter(Fields.BOOK_PUBLISHER + " English"));
        book.setPublisherUa(request.getParameter(Fields.BOOK_PUBLISHER + " Ukrainian"));
        book.setName(request.getParameter(Fields.ENTITY_NAME + " English"));
        book.setNameUa(request.getParameter(Fields.ENTITY_NAME + " Ukrainian"));
        return book;
    }
}
