<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@taglib prefix="f" uri="http://library.varenyk_mykyta.ntudp.dnipro.ua/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="с" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>Admin/Books</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
</head>
<body>
<div class="page-wrapper">
    <div class="container">

        <fmt:message key="book_jsp.createNewBook"/>
    <div class="col s4">
        <div class="show-create-form">
            <form action="${pageContext.request.contextPath}/controller/admin/create-book" method="post">
                <label for="author">Author/s:</label>
                <select class="browser-default" id="author" name="authorId">
                    <c:forEach var="author" items="${requestScope.authors}">
                        <option value="${author.id}">${author.name}</option>
                    </c:forEach>
                </select>
                <br>

                <с:forEach var="language" items="${requestScope.languages}">
                    <label for="${f:concat('publisher',language)}">publisher <jsp:text>${language}</jsp:text>: </label>
                    <input class="browser-default" type="text" id="${f:concat('publisher',language)}" name="${f:concat('publisher',language)}"
                           required
                           maxlength="255">
                    <br>

                    <label for="${f:concat('name', language)}">name <jsp:text>${language}</jsp:text>: </label>
                    <input class="browser-default" type="text" id="${f:concat('name', language)}" name="${f:concat('name', language)}"
                           required
                           maxlength="255">
                    <br>

                    <label for="${f:concat('description', language)}">description <jsp:text>${language}</jsp:text>: </label>
                    <input class="browser-default" type="text" id="${f:concat('description', language)}" name="${f:concat('description', language)}"
                           required
                           maxlength="255">
                    <br>
                </с:forEach>

                <c:if test="${sessionScope.dateOfIssueInTheFuture != null}">
                    ${sessionScope.dateOfIssueInTheFuture}
                    <c:remove var="dateOfIssueInTheFuture" scope="session"/><br/>
                </c:if>
                <label for="date_of_issue">date of issue: </label>
                <input class="browser-default" type="date" id="date_of_issue" name="date_of_issue"
                       min="0000-01-01" max="2030-09-30">
                <br>


                <label for="count">count: </label>
                <input id="count" type="number" min="1" max="1500" required
                       placeholder="1-1500" name="count">
                <br>
                <input type="submit" class="btn" value="create"/>
            </form>
        </div>
    </div>
         <div class="row">
    <%--<div class="col s8">--%>
<%--<div class="list-of-books">--%>
        <c:forEach var="book" items="${requestScope.books}">
          <div class="book-item">
            <div class="row">
                <div class="col s9">
                    <div class="row">
                       <div class="col s3">
                         <fmt:message key="label.name"/>
                          <span>${book.name}</span>
                       </div>
                    </div>

            <div class="row">
              <div class="col s3">
                  <jsp:text>Author/s:</jsp:text>
                <span><c:forEach var="authorVar" items="${book.author}">${authorVar}</c:forEach></>
                </div>
            </div>
            <div class="row">
              <div class="col s3">
              <span><fmt:message key="label.dateOfIssue"/>: ${book.dateOfIssue}</span>
               </div>
            </div>
           <div class="row">
               <div class="col s3">
               <span><fmt:message key="label.publisher"/>: ${book.publisher}</span>
               </div>
           </div>
              <div class="row">
            <div class="col s3">
                <span><fmt:message key="label.description"/>: ${book.description}</span>
            </div>
             </div>
        <div class="row">
            <div class="col s3">
              <span><fmt:message key="label.count"/>: ${book.count}</span>
             </div>
        </div>
            </div><br/>
            <div class="col s3">
                 <form method="post"
                       action="${pageContext.request.contextPath}/controller/admin/delete-book">
                   <input type="number" hidden name="id" value="${book.id}"/>
                    <input type="submit" class="btn"
                          value="<fmt:message key="button.delete"/>"/>
                 </form>
           </div>
           <div class="col s3">
               <form method="post"
                     action="${pageContext.request.contextPath}/controller/admin/edit-book">
                         <input type="number" hidden name="id" value="${book.id}"/>
                         <input type="submit" class="btn"
                            value="<fmt:message key="button.edit"/>"/>
               </form>
           </div>
          </div>
     </div><br/>
</c:forEach>
    </div>
    <br/>
    <ul class="pagination">
        <c:if test="${requestScope.currentPage != 1}">
            <li class="disabled">
                <a href="${pageContext.request.contextPath}/controller/admin/books?page=${requestScope.currentPage-1}">                                        Previous
                </a>
            </li>
        </c:if>

        <c:forEach begin="1" end="${requestScope.numOfPages}" varStatus="i">
            <c:choose>
                <c:when test="${requestScope.currentPage eq i.index}}">
                    <li class="page-active">
                        <a class="page-link">
                                ${i.index}<span class="sr-only">(current)</span>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-active">
                        <a class="page-link" href="${pageContext.request.contextPath}/controller/admin/books?page=${i.index}">${i.index}</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>

        <c:if test="${requestScope.currentPage lt requestScope.numOfPages}">
            <li class="page-item">
                <a class="page-link" href="${pageContext.request.contextPath}/controller/admin/books?page=${requestScope.currentPage+1}">Next</a>
            </li>
        </c:if>
    </ul>
    </div>
     </div>
</div>
<h:change-language path="redirect:/FinalProject/controller/admin/books"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
    </body>
</html>