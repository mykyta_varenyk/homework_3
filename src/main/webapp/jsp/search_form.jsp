<table id="main-container">
    <tr>
        <td class="content center">
            <form action="${pageContext.request.contextPath}/controller/search" method="get">
                <fmt:message key="label.searchTranslation"/>
                <br>
                <fieldset>
                    <legend>
                        <fmt:message key="label.authorTranslation"/>
                    </legend>
                    <select class="browser-default" id="author" name="author-id">
                        <c:forEach var="author" items="${sessionScope.authors}">
                            <option value="${author.name}">${author.name}</option>
                        </c:forEach>
                    </select>
                </fieldset>
                <input type="checkbox" class="check-box" name="search-by-author" value="yes"><br>
                <fieldset>
                    <legend>
                        <fmt:message key="label.nameTranslation"/>
                    </legend>
                    <select class="browser-default" id="name" name="name-id">
                        <c:forEach var="book" items="${sessionScope.booksList}">
                            <option value="${book.name}">${book.name}</option>
                        </c:forEach>
                    </select>
                </fieldset>
                <input type="checkbox" name="search-by-name" value="yes">
                <input type="submit" class="btn btn-primary mt-3 find" value='<fmt:message key="button.findTranslation"/>'/>
            </form>
        </td>
    </tr>
</table>