<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:text>Make order: </jsp:text>
<div class="col s4">
    <div class="show-create-form">
        <form action="${pageContext.request.contextPath}/controller/user/confirm-take-home-order" method="post">
            <input type="number" hidden name="id" value="${requestScope.id}">

            <label for="days_count">days amount: </label>
            <input id="days_count" type="number" min="1" max="1500" required
                   placeholder="0-1500" name="days_count">
            <br>
            <input type="submit" class="btn" value="confirm"/>
        </form>
    </div>
</div>
</div>
</body>
</html>
