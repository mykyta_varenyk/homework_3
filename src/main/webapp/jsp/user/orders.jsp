<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@taglib prefix="f" uri="http://library.varenyk_mykyta.ntudp.dnipro.ua/functions" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>User/Orders</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/css/index.css">
</head>
<div class="wrapper">
<%@include file="/WEB-INF/jspf/header.jspf"%>
<main>
    <div class="page-wrapper wrapper">
        <div class="container">
            <div class="row">
                <c:forEach var="bean" items="${requestScope.userOrderBeans}">
                <c:if test="${bean.approvedTime != null}">
                    <div class="order-item">
                        <div class="row">
                            <div class="col s9">
                                <div class="row">
                                    <div class="col s3">
                                        ID:
                                        <span>${bean.orderId}</span>
                                    </div>
                                </div>
                                    <div class="row">
                                            <div class="col s3">
                                                <fmt:message key="label.name"/>
                                                <span>${bean.name}</span>
                                            </div>
                                    </div>

                                <div class="row">
                                    <div class="col s3">
                                        <fmt:message key="label.publisher"/>
                                        <span>${bean.publisher}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s3">
                                        <fmt:message key="label.author"/>
                                        <span>${bean.author}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s3">
                                        <fmt:message key="orders_jsp.approvedByLibrarian"/>
                                        <span>${bean.librarianId}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <c:choose>
                                        <c:when test="${bean.returned == true}">
                                            <span>returned</span>
                                        </c:when>
                                        <c:when test="${bean.daysCount != 0}">
                                            <c:set var="delay" value="${f:delay(bean.approvedTime)}"/>
                                            <div class="col s3">
                                                <c:choose>
                                                    <c:when test="${delay >= bean.daysCount}">
                                                        <fmt:message key="label.penaltySize"/>
                                                        <span>${10 * (delay - bean.daysCount)}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="label.daysCount"/>
                                                        <span>${bean.daysCount - delay}</span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="delay" value="${f:delayHours(bean.approvedTime)}"/>
                                            <div class="col s3">
                                                <c:choose>
                                                    <c:when test="${delay > bean.hoursCount}">
                                                        <fmt:message key="label.penaltySize"/>
                                                        <span>${3*(delay - bean.hoursCount)}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="label.hoursCount"/>
                                                        <span>${bean.hoursCount - delay}</span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="row">
                                    <div class="col s3">
                                        <form method="post"
                                              action="${pageContext.request.contextPath}/controller/user/return-book">
                                            <input type="number" hidden name="id" value="${bean.orderId}">
                                            <input type="submit" class="btn" value="return">
                                        </form><br/>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                    </div><br/>
            </c:if>
                </c:forEach>
            <ul class="pagination">
                <c:if test="${requestScope.currentPage != 1}">
                    <li class="disabled">
                        <a class="page-link previous" href="${pageContext.request.contextPath}/controller/user/orders?page=${requestScope.currentPage-1}">                                        Previous
                        </a>
                    </li>
                </c:if>

                <c:forEach begin="1" end="${requestScope.numOfPages}" varStatus="i">
                    <c:choose>
                        <c:when test="${requestScope.currentPage eq i.index}}">
                            <li class="page-active">
                                <a class="page-link">
                                        ${i.index}<span class="sr-only">(current)</span>
                                </a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="page-active">
                                <a class="page-link" href="orders?page=${i.index}">${i.index}</a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>

                <c:if test="${requestScope.currentPage lt requestScope.numOfPages}">
                    <li class="page-item">
                        <a class="page-link" href="${pageContext.request.contextPath}/controller/user/orders?page=${requestScope.currentPage+1}">Next</a>
                    </li>
                </c:if>
            </ul>
            <h:change-language path="redirect:/FinalProject/controller/user/orders"/>
            <%@include file="/WEB-INF/jspf/footer.jspf"%>
            </div>
        </div><br/>
</main>
</div>

</body>
</html>
