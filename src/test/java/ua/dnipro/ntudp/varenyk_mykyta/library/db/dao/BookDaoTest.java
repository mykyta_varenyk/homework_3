package ua.dnipro.ntudp.varenyk_mykyta.library.db.dao;

import org.junit.Before;
import org.junit.Test;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.sql.DataSource;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class BookDaoTest {
    private DataSource dataSource;

    private BookDao bookDao;

    @Before
    public void setUp(){
        dataSource = DBManager.getDataSourceForTests();
        bookDao = new BookDao(dataSource);
    }

    @Test
    public void checkNumberOfBooks(){
        int numberOfBooks = bookDao.getNumberOfBooks();

        assertEquals(numberOfBooks,bookDao.getNumberOfBooks());
    }

    @Test
    public void getBooksWithLimitAndOffset(){
        String languageCode="en";

        int currentPage = 1;

        int recordsPerPage = 2;

        List<Book> books = bookDao.getBooks(currentPage,recordsPerPage,languageCode);

        assertEquals(books.size(),bookDao.getBooks(currentPage,recordsPerPage,languageCode).size());
    }

    @Test
    public void shouldReturnAllAuthors(){
        String languageCode = "en";

        List<Author> authors = bookDao.getAllAuthors(languageCode);

        assertEquals(authors.size(),bookDao.getAllAuthors(languageCode).size());
    }

    @Test
    public void shouldReturnAllBookNames(){
        String languageCode = "en";

        List<Book> bookNames = bookDao.getAllBookNames(languageCode);

        assertEquals(bookNames.size(),bookDao.getAllBookNames(languageCode).size());
    }

    @Test
    public void shouldReturnAllPublishers(){
        String languageCode = "en";

        List<String> publishers = bookDao.getAllPublishers(languageCode);

        assertEquals(publishers.size(),bookDao.getAllPublishers(languageCode).size());
    }

    @Test
    public void shouldReturnAllDatesOfIssue(){
        List<LocalDateTime> datesOfIssue = bookDao.getAllDatesOfIssue();

        assertEquals(datesOfIssue.size(),bookDao.getAllDatesOfIssue().size());
    }

    @Test
    public void shouldReturnAuthorById(){
        String languageCode = "en";

        int id = 2;

        String authorName = bookDao.getAuthorById(id,languageCode);

        assertEquals(authorName,bookDao.getAuthorById(id,languageCode));
    }

    @Test
    public void shouldReturnBookById(){
        String languageCode = "en";

        int id = 3;

        Book book = bookDao.getBookById(id,languageCode);

        assertEquals(book.getId(),bookDao.getBookById(id,languageCode).getId());
    }

    @Test
    public void shouldFindBookBeansByNameAndAuthor(){
        String languageCode = "en";

        String author = "John R.R. Tolkien";

        String name = "The Lord of The Rings";

        List<Book> bookBeans = bookDao.findBookBeansByNameAndAuthor(languageCode,author,name);

        assertEquals(bookBeans.size(),bookDao.findBookBeansByNameAndAuthor(languageCode,author,name).size());
    }

    @Test
    public void shouldFindBookBeansByName(){
        String languageCode = "en";

        String name = "The Lord of The Rings";

        List<Book> bookBeans = bookDao.findBookBeansByName(languageCode,name);

        assertEquals(bookBeans.size(),bookDao.findBookBeansByName(languageCode,name).size());
    }

    @Test
    public void shouldFindBookBeansByAuthor(){
        String languageCode = "en";

        String author = "John R.R. Tolkien";

        List<Book> bookBeans = bookDao.findBookBeansByAuthors(languageCode,author);

        assertEquals(bookBeans.size(),bookDao.findBookBeansByAuthors(languageCode,author).size());
    }

    @Test
    public void shouldReturnAllBooks(){
        String languageCode = "en";

        List<Book> books = bookDao.getAllBooks(languageCode);

        assertEquals(books.size(),bookDao.getAllBooks(languageCode).size());
    }

   /* @Test
    public void shouldReturnAllBookBeans(){
        String languageCode = "en";

        List<Book> books = bookDao.getAllBookBeans(languageCode);

        assertEquals(books.size(),bookDao.getAllBooks(languageCode).size());
    }*/

    @Test
    public void shouldCreateAndDeleteBook(){
        Book book = new Book();

        book.setDate("2020-07-07");

        book.setCount(3);

        book.setName("Another name");

        book.setNameUa("Інше ім'я");

        book.setPublisher("Publisher");

        book.setPublisherUa("Видавець");

        assertTrue(bookDao.createBook(book,4));

        List<Book> books = bookDao.getAllBooks("en");

        book.setId(books.get(books.size()-1).getId());

        assertTrue(bookDao.deleteBookById(book.getId()));
    }

    @Test
    public void shouldCreateAndUpdateAndThanDeleteBook(){
        Book book = new Book();

        String languageCode = "en";

        book.setDate("2020-07-07");

        book.setCount(3);

        book.setName("Another name");

        book.setNameUa("Інше ім'я");

        book.setPublisher("Publisher");

        book.setPublisherUa("Видавець");

        assertTrue(bookDao.createBook(book,4));

        List<Book> books = bookDao.getAllBooks(languageCode);



        System.out.println(bookDao.getBookById(books.get(books.size()-1).getId(),languageCode));

        assertTrue(bookDao.updateBook(bookDao.getBookById(books.get(books.size()-1).getId(),languageCode),
                1));

        books = bookDao.getAllBooks(languageCode);

        book.setId(books.get(books.size()-1).getId());

        assertTrue(bookDao.deleteBookById(book.getId()));
    }
}