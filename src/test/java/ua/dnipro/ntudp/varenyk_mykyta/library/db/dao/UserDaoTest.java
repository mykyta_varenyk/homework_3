package ua.dnipro.ntudp.varenyk_mykyta.library.db.dao;

import org.junit.Before;
import org.junit.Test;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.naming.NamingException;
import javax.sql.DataSource;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class UserDaoTest {
    private DataSource dataSource;
    private UserDao userDao;

    @Before
    public void setUp() throws NamingException {
        dataSource = DBManager.getDataSourceForTests();
        userDao = new UserDao(dataSource);
    }

    @Test
    public void checkCorrectNumberOfUsers(){
        int numberOfUsers = userDao.getNumberOfUsers();

        assertEquals(numberOfUsers,userDao.getNumberOfUsers());
    }

    @Test
    public void getUsersWithLimitAndOffset(){
        int currentPage = 1;
        int recordsPerPage = 2;
        List<User> users = userDao.getUsers(currentPage,recordsPerPage);

        assertEquals(users.size(),userDao.getUsers(currentPage,recordsPerPage).size());
    }

    @Test
    public void shouldReturnUserWithSpecifiedLogin(){
        String login = "mykytavar";

        User user1 = userDao.findUserByLogin(login);

        assertEquals(user1.getLogin(),userDao.findUserByLogin(login).getLogin());
    }

    @Test
    public void checkIfEmailIsPresent(){
        String email = "mykytavar@gmail.com";

        assertTrue(userDao.emailIsPresent(email));
    }

    @Test
    public void checkIfEmailIsNotPresent(){
        String email = "m";

        assertFalse(userDao.emailIsPresent(email));
    }

    @Test
    public void checkIfLoginIsPresent(){
        String login = "mykytavar";

        assertTrue(userDao.loginIsPresent(login));
    }

    @Test
    public void shouldBlockAndUnblockUser(){
        int id = 1;

        assertTrue(userDao.blockUser(id));

        assertTrue(userDao.unblockUser(id));
    }

    @Test
    public void shouldCreateAndDeleteLibrarian(){
        User user = new User();
        user.setLogin("login");
        user.setEmail("example@examle.com");
        user.setPassword("pass");
        assertTrue(userDao.createLibrarian(user));
        assertTrue(userDao.deleteLibrarian(userDao.findUserByLogin(user.getLogin()).getId()));
    }

    @Test
    public void shouldReturnAllUsers(){
        List<User> users = userDao.getAllUsers();

        assertEquals(users.size(),userDao.getAllUsers().size());
    }

    @Test
    public void shouldCreateUser(){
        Random random = new Random();
        User user = new User();

        user.setLogin(String.valueOf((char)random.nextInt(9)));

        user.setPassword("test");

        user.setRoleId(2);

        assertTrue(userDao.createUser(user));
    }
}