package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.junit.Test;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

public class GetOrdersForUserPageCommandTest {
    @Test
    public void shouldReturnCorrectPath(){
        GetOrdersForUserPageCommand command = new GetOrdersForUserPageCommand(new OrderDao(DBManager.getDataSourceForTests()));

        HttpServletRequest request = mock(HttpServletRequest.class);

        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getParameter("page")).thenReturn(String.valueOf(1));

        when(request.getParameter("id")).thenReturn(String.valueOf(1));

        assertEquals(Path.PAGE_LIBRARIAN_LIST_ORDERS_FOR_USER,command.execute(request,response));
    }
}