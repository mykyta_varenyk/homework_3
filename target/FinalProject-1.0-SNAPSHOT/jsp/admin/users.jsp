<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>Users</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
</head>
<body>
<div class="page-wrapper">
    <div class="container">
        <fmt:message key="users_jsp.createLibrarian"/>
        <div class="col s4">
            <div class="show-create-form">
                <form action="${pageContext.request.contextPath}/controller/admin/create-librarian" method="post">

                    <c:if test="${sessionScope.loginIsPresentError != null}">
                        ${sessionScope.loginIsPresentError}
                        <c:remove var="loginIsPresentError" scope="session"/><br/>
                    </c:if>
                    <label for="login"><fmt:message key="label.login"/>: </label>
                    <input class="browser-default" type="text" id="login" name="login"
                           required
                           maxlength="30">
                    <br>

                    <c:choose>
                        <c:when test="${sessionScope.emailPatternError != null}">
                            ${sessionScope.emailPatternError}
                            <c:remove var="emailPatternError" scope="session"/><br/>
                        </c:when>
                        <c:when test="${sessionScope.emailIsPresentError != null}">
                            ${sessionScope.emailIsPresentError}
                            <c:remove var="emailIsPresentError" scope="session"/><br/>
                        </c:when>
                    </c:choose>
                    <label for="email"><fmt:message key="label.email"/>: </label>
                    <input class="browser-default" type="text" id="email" name="email"
                           required
                           maxlength="30" placeholder="example@example.com">
                    <br>

                    <c:if test="${sessionScope.passwordPatternError != null}">
                        ${sessionScope.passwordPatternError}
                        <c:remove var="passwordPatternError" scope="session"/><br/>
                    </c:if>
                    <label for="password"><fmt:message key="label.password"/>: </label>
                    <input class="browser-default" type="password" id="password" name="password"
                           required
                           maxlength="30">
                    <br>

                    <label for="first_name"><fmt:message key="label.firstName"/>: </label>
                    <input class="browser-default" type="text" id="first_name" name="first_name"
                           required
                           maxlength="30">
                    <br>

                    <label for="last_name"><fmt:message key="label.lastName"/>: </label>
                    <input class="browser-default" type="text" id="last_name" name="last_name"
                           required
                           maxlength="30">
                    <br>
                    <input type="submit" class="btn" value="<fmt:message key="button.create"/>"/>
                </form>
            </div>
        </div>
        <div class="row">
            <c:forEach var="user" items="${requestScope.users}">
                <div class="user-item">
                    <div class="row">
                        <div class="col s9">
                            <div class="row">
                                <div class="col s3">
                                    ID:
                                    <span>${user.id}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s6">
                                    <fmt:message key="label.email"/>
                                    <span>${user.email}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.login"/>
                                    <span>${user.login}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.role"/>
                                    <span>${user.role}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.firstName"/>
                                    <span>${user.firstName}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3">
                                    <fmt:message key="label.lastName"/>
                                    <span>${user.lastName}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col3">

                            <c:choose>
                                <c:when test="${user.blocked == false and user.roleId == 2}">
                                    <form class="block-user" method="post"
                                          action="${pageContext.request.contextPath}/controller/admin/block-user">
                                        <input type="number" hidden name="id" value="${user.id}"/>
                                        <input type="submit" class="btn"
                                               value="<fmt:message key="button.block"/>"/>
                                    </form>
                                </c:when>
                                <c:when test="${user.blocked == true}">
                                    <form class="unblock-user" method="post"
                                          action="${pageContext.request.contextPath}/controller/admin/unblock-user">
                                        <input type="number" hidden name="id" value="${user.id}"/>
                                        <input type="submit" class="btn"
                                               value="<fmt:message key="button.unblock"/>"/>
                                    </form>
                                </c:when>
                                <c:when test="${user.roleId == 3}">
                                    <form class="delete-librarian" method="post"
                                          action="${pageContext.request.contextPath}/controller/admin/delete-librarian">
                                        <input type="number" hidden name="id" value="${user.id}">
                                        <input type="submit" class="btn"
                                        value="<fmt:message key="button.delete"/>">
                                    </form>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                </div><br/>
            </c:forEach>
        </div>
        <br/>
        <ul class="pagination">
            <c:if test="${requestScope.currentPage != 1}">
                <li class="disabled">
                    <a href="${pageContext.request.contextPath}/controller/admin/users?page=${requestScope.currentPage-1}">                                        Previous
                    </a>
                </li>
            </c:if>

            <c:forEach begin="1" end="${requestScope.numOfPages}" varStatus="i">
                <c:choose>
                    <c:when test="${requestScope.currentPage eq i.index}}">
                        <li class="page-active">
                            <a class="page-link">
                                    ${i.index}<span class="sr-only">(current)</span>
                            </a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-active">
                            <a class="page-link" href="${pageContext.request.contextPath}/controller/admin/users?page=${i.index}">${i.index}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:if test="${requestScope.currentPage lt requestScope.numOfPages}">
                <li class="page-item">
                    <a class="page-link" href="${pageContext.request.contextPath}/controller/admin/users/page=${requestScope.currentPage+1}">Next</a>
                </li>
            </c:if>
        </ul>

    </div>
    </div><br/>
    </div>
</div>
<h:change-language path="redirect:/FinalProject/controller/admin/users"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
