<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>
<html>
<head>
    <title>Librarian home page</title>
</head>
<body>
<fmt:message key="label.userGreeting"/>, ${sessionScope.user.login}
<br/><br/><br/>

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="user-item">
                <div class="row">
                    <div class="col s9">
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.email"/>
                                <span>${sessionScope.user.email}</span>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.login"/>
                                <span>${sessionScope.user.login}</span>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.firstName"/>
                                <span>${sessionScope.user.firstName}</span>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="col s3">
                                <fmt:message key="label.lastName"/>
                                <span>${sessionScope.user.lastName}</span>
                            </div>
                        </div><br/>
                    </div>
                </div><br/>

                <form method="post" action="${pageContext.request.contextPath}/controller/librarian/orders">
                    <input class="btn" type="submit" value="Show orders">
                </form><br/>

                <form method="post" action="${pageContext.request.contextPath}/controller/librarian/users">
                    <input class="btn" type="submit" value="Show readers">
                </form>
            </div>
        </div>
    </div>
</div>

<h:change-language path="redirect:/FinalProject/controller/librarian/home"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>

</body>
</html>
