<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fmt:setBundle basename="text"/>

<html>
<head>
    <title>register</title>
</head>
<body>
<h1>Register</h1>
<table id="main-controller">
    <tr>
        <td class="content center">
            <form id="register_form" action="${pageContext.request.contextPath}/controller/register" method="post">
                <input type="hidden" name="command" value="register"/>

                <c:if test="${sessionScope.loginIsPresentError != null}">
                    ${sessionScope.loginIsPresentError}
                    <c:remove var="loginIsPresentError" scope="session"/><br/>
                </c:if>
                <fieldset>
                    <legend>
                        <fmt:message key="register_jsp.login"/>
                    </legend>
                    <input name="login"/>
                </fieldset><br/>

                <c:if test="${sessionScope.passwordPatternError != null}">
                    ${sessionScope.passwordPatternError}
                    <c:remove var="passwordPatternError" scope="session"/><br/>
                </c:if>
                <fieldset>
                    <legend>
                        <fmt:message key="register_jsp.password"/>
                    </legend>
                    <input type="password" name="password"/>
                </fieldset><br/>

                <c:choose>
                    <c:when test="${sessionScope.emailPatternError != null}">
                        ${sessionScope.emailPatternError}
                        <c:remove var="emailPatternError" scope="session"/><br/>
                    </c:when>
                    <c:when test="${sessionScope.emailIsPresentError != null}">
                        ${sessionScope.emailIsPresentError}
                        <c:remove var="emailIsPresentError" scope="session"/><br/>
                    </c:when>
                </c:choose>
                <fieldset>
                    <legend>
                        <fmt:message key="register_jsp.email"/>
                    </legend>
                    <input type="email" name="email"/>
                </fieldset><br/>

                <fieldset>
                    <legend>
                        <fmt:message key="register_jsp.firstName"/>
                    </legend>
                    <input type="text" name="first_name"/>
                </fieldset><br/>

                <fieldset>
                    <legend>
                        <fmt:message key="register_jsp.lastName"/>
                    </legend>
                    <input type="text" name="last_name"/>
                </fieldset><br/>
                <input type="submit" value="<fmt:message key="label.registerTranslation"/>">
            </form>
        </td>
    </tr>
</table>

<h:change-language path="/register.jsp"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
